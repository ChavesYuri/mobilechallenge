//
//  CoinListRequestModel.swift
//  BTGConversion
//
//  Created by Yuri Chaves on 19/12/20.
//  Copyright © 2020 Yuri Chaves. All rights reserved.
//

import Foundation

class CoinListRequestModel: RequestModel {
    
    override var path: String{
        return "/list"
    }
   
}
