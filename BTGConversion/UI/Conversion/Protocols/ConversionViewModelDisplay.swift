//
//  ConversionViewModelDisplay.swift
//  BTGConversion
//
//  Created by Yuri Chaves on 21/12/20.
//  Copyright © 2020 Yuri Chaves. All rights reserved.
//

import Foundation

protocol ConversionViewModelDisplay {
    func displayResult(result: String)
}
